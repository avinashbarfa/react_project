import { applyMiddleware, combineReducers, createStore } from "redux";
import { Dishes } from "./dishes";
import { Comments } from "./comment";
import { Promotions } from "./promotions";
import { Leaders } from "./leaders";
import { logger } from "redux-logger/src";
import thunk from "redux-thunk";
import { createForms } from "react-redux-form";
import { InitialFeedback } from "./Form";

export const ConfigureStore = () => {
  return createStore(
    combineReducers({
      dishes: Dishes,
      comments: Comments,
      promotions: Promotions,
      leaders: Leaders,
      ...createForms({
        feedback: InitialFeedback,
      }),
    }),
    applyMiddleware(thunk, logger)
  );
};
