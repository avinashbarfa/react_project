import React from "react";
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
} from "reactstrap";
import { Loading } from "./LoadingComponent";
import { baseUrl } from "../shared/BaseUrl";

function RenderCard({ item, isLoading, errMsg }) {
  if (isLoading) {
    return <Loading />;
  } else if (errMsg) {
    return <h4>{errMsg}</h4>;
  }

  return (
    <Card>
      <CardImg src={baseUrl + item.image} alt={item.name} />
      <CardBody>
        <CardTitle>{item.name}</CardTitle>
        {item.designation ? (
          <CardSubtitle>{item.designation}</CardSubtitle>
        ) : null}
        <CardText>{item.description}</CardText>
      </CardBody>
    </Card>
  );
}

function HomeComponent(props) {
  return (
    <div className="container">
      <div className="row align-items-start">
        <div className="col-lg-4 col-md-4">
          <RenderCard
            item={props.dish}
            isLoading={props.dishesLoading}
            errMsg={props.dishesErrMsg}
          />
        </div>
        <div className="col-lg-4 col-md-4">
          <RenderCard
            item={props.promotion}
            isLoading={props.promosLoading}
            errMsg={props.promosErrMsg}
          />
        </div>
        <div className="col-lg-4 col-md-4">
          <RenderCard
            item={props.leader}
            isLoading={props.leaderLoading}
            errMsg={props.leaderErrMsg}
          />
        </div>
      </div>
    </div>
  );
}

export default HomeComponent;
