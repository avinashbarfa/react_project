import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  CardTitle,
  Row,
  Label,
  List,
  Modal,
  ModalBody,
  ModalHeader,
  Col,
} from "reactstrap";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Control, Errors, LocalForm } from "react-redux-form";
import { Loading } from "./LoadingComponent";
import { baseUrl } from "../shared/BaseUrl";

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !val || val.length <= len;
const minLength = (len) => (val) => val && val.length >= len;

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPostCommentModalOpen: false,
    };
    this.togglePostCommentModal = this.togglePostCommentModal.bind(this);
    this.handlePostComment = this.handlePostComment.bind(this);
  }

  togglePostCommentModal() {
    this.setState({
      isPostCommentModalOpen: !this.state.isPostCommentModalOpen,
    });
  }

  handlePostComment(values) {
    this.togglePostCommentModal();
    this.props.postComment(
      this.props.dishId,
      values.rating,
      values.author,
      values.comment
    );
  }

  render() {
    return (
      <>
        <Button outline onClick={this.togglePostCommentModal}>
          <span className="fa fa-edit fa-lg" /> Submit Comment
        </Button>
        <Modal
          isOpen={this.state.isPostCommentModalOpen}
          toggle={this.togglePostCommentModal}
        >
          <ModalHeader toggle={this.togglePostCommentModal}>
            Submit Comment
          </ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={(values) => this.handlePostComment(values)}>
              <Row className={"form-group"}>
                <Label htmlFor="rating" md={4}>
                  Rating
                </Label>
                <Col md={12}>
                  <Control.select
                    model=".rating"
                    id="rating"
                    name="rating"
                    className="form-control"
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className={"form-group"}>
                <Label htmlFor="author" md={4}>
                  Your Name
                </Label>
                <Col md={12}>
                  <Control.text
                    model=".author"
                    id={"author"}
                    name={"author "}
                    className="form-control"
                    placeholder={"Your Name"}
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15),
                    }}
                  />
                  <Errors
                    className="text-danger"
                    model=".author"
                    show="touched"
                    messages={{
                      required: "Required",
                      minLength: "Must be greater than 2 characters",
                      maxLength: "Must be 15 characters or less",
                    }}
                  />
                </Col>
              </Row>
              <Row className={"form-group"}>
                <Label htmlFor="comment" md={4}>
                  Comment
                </Label>
                <Col md={12}>
                  <Control.textarea
                    model={".comment"}
                    id={"comment"}
                    name={"comment"}
                    row="6"
                    className={"form-control"}
                  />
                </Col>
              </Row>
              <Button type={"submit"} value={"submit"} className={"bg-primary"}>
                Submit
              </Button>
            </LocalForm>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

function RenderComments({ comments, postComment, dishId }) {
  const commentSection = comments.map((comment) => {
    const date = new Intl.DateTimeFormat("en-us", {
      year: "numeric",
      month: "short",
      day: "2-digit",
    }).format();
    return (
      <div>
        <List>{comment.comment}</List>
        <List>
          -- {comment.author}, {date}
        </List>
      </div>
    );
  });
  return (
    <div className="col-md-6">
      <List type="inline">
        <h2>Comments</h2>
        {commentSection}
      </List>
      <CommentForm dishId={dishId} postComment={postComment} />
    </div>
  );
}

function RenderDish({ dish }) {
  if (dish) {
    return (
      <div key={dish.id} className="col-md-6">
        <Card>
          <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
          <CardBody>
            <CardTitle>{dish.name}</CardTitle>
            <CardText>{dish.description}</CardText>
          </CardBody>
        </Card>
      </div>
    );
  }
}

const DishDetail = (props) => {
  const { dish, comments, postComment, isLoading, errMsg } = props;

  if (isLoading) {
    return (
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  } else if (errMsg) {
    return (
      <div className="container">
        <div className="row">
          <h4>{errMsg}</h4>
        </div>
      </div>
    );
  }

  if (dish && comments) {
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to={"/home"}>Home</Link>
            </BreadcrumbItem>
            <BreadcrumbItem>
              <Link to={"/menu"}>Menu</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>{dish.name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-md-12">
            <h3>{dish.name}</h3>
            <hr />
          </div>
          <RenderDish dish={dish} />
          <RenderComments
            comments={comments}
            postComment={postComment}
            dishId={dish.id}
          />
        </div>
      </div>
    );
  }
  return <div />;
};

export default DishDetail;
